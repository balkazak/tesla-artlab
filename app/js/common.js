$(function() {


	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});

	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(location).attr('href', '/thanks.html');
		});
		return false;
	});

	var swiperHeader = new Swiper('.swiper-main-header', {
		speed: 400,
		navigation: {
			nextEl: '.swiper-main-next',
			prevEl: '.swiper-main-prev',
		}
	});

	var swiperTeam = new Swiper('.swiper-team', {
		speed: 400,
		spaceBetween: 50,
		slidesPerView: 3,
		navigation: {
			nextEl: '.swiper-team-next',
			prevEl: '.swiper-team-prev',
		}
	});

});

